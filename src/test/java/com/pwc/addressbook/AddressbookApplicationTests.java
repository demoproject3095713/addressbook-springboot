package com.pwc.addressbook;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.pwc.addressbook.dao.AddressBookDao;
import com.pwc.addressbook.exception.ContactAlreadyExistsException;
import com.pwc.addressbook.exception.NoSuchAddressBookExistsException;
import com.pwc.addressbook.model.Friend;
import com.pwc.addressbook.repository.FriendRepository;
import com.pwc.addressbook.service.AddressBookServiceImpl;


@ExtendWith(MockitoExtension.class)
public class AddressbookApplicationTests {

	@Mock
    private FriendRepository friendRepository;
    @Spy
    private ModelMapper modelMapper;
    @InjectMocks
    private AddressBookServiceImpl addressBookServiceImpl;

    @Test
    void addFriendTest() {
        String response = "SUCCESS,friend's name Saved";

        AddressBookDao addressBookDao = new AddressBookDao();
        addressBookDao.setName("Name");
        addressBookDao.setAddressBookName("AddressBook");
        Optional<Friend> optionalAddressBook = Optional.empty();
        when(friendRepository.findByNameAndAddressBookName(Mockito.anyString(), Mockito.anyString())).thenReturn(optionalAddressBook);
        Assertions.assertEquals(response, addressBookServiceImpl.addFriend(addressBookDao));
    }
    
    @Test
    void addFriendTest_throwsException() {
        AddressBookDao addressBookDao = new AddressBookDao();
        addressBookDao.setName("Name");
        addressBookDao.setAddressBookName("AddressBook");
        Friend addressBook = new Friend();
        addressBook.setName("NameNew");
        addressBook.setAddressBookName("AddressBookNew");
        Optional<Friend> optionalAddressBook = Optional.ofNullable(addressBook);
        when(friendRepository.findByNameAndAddressBookName(Mockito.anyString(), Mockito.anyString())).thenReturn(optionalAddressBook);
        Assertions.assertThrows(ContactAlreadyExistsException.class, () -> {
            addressBookServiceImpl.addFriend(addressBookDao);
        });

    }
    
    @Test
    void getAllFriendsTest() {

        Friend friend = new Friend(1, "Lio", "AddressBook1", "9876543210");
        List<Friend> testList = new ArrayList<>();
        testList.add(friend);
        when(friendRepository.findAll()).thenReturn(testList);
        List<AddressBookDao> li = addressBookServiceImpl.getAllFriends();
        Assertions.assertNotNull(addressBookServiceImpl.getAllFriends());

        Assertions.assertEquals("Lio", li.get(0).getName());
    }

    @Test
    void getAllUniqueFriendsTest() {
        String addressBookName1 = "AddressBook1";
        String addressBookName2 = "AddressBook2";
        Friend addressBook = new Friend();
        List<Friend> addressBookList = new ArrayList<>();
        addressBookList.add(addressBook);
        when(friendRepository.findByAddressBookName(Mockito.anyString())).thenReturn(addressBookList);
        Assertions.assertNotNull(addressBookServiceImpl.getAllUniqueFriends(addressBookName1, addressBookName2));
    }

    @Test
    void getAllFriendsByAddressBookNameTest() {
        String addressBookName = "Name";
        Friend addressBook = new Friend();
        List<Friend> addressBookList = new ArrayList<>();
        addressBookList.add(addressBook);
        when(friendRepository.findAllFriendsByAddressBookName(Mockito.anyString())).thenReturn(addressBookList);
        Assertions.assertNotNull(addressBookServiceImpl.getAllFriendsByAddressBookName(addressBookName));
    }

    @Test
    void getAllFriendsByAddressBookNameTest_throwsException() {
        String addressBookName = "Name";
        List<Friend> addressBookList = new ArrayList<>();
        when(friendRepository.findAllFriendsByAddressBookName(Mockito.anyString())).thenReturn(addressBookList);
        Assertions.assertThrows(NoSuchAddressBookExistsException.class, () -> {
            addressBookServiceImpl.getAllFriendsByAddressBookName(addressBookName);
        });
    }

}
