package com.pwc.addressbook.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pwc.addressbook.dao.AddressBookDao;
import com.pwc.addressbook.model.Friend;
import com.pwc.addressbook.service.AddressBookService;

@Controller
@RequestMapping("/friend/v1")
public class AddressBookController {
	
	 private static final Logger log = LoggerFactory.getLogger(AddressBookController.class);
	    @Autowired
	    private AddressBookService addressBookService;

	    @PostMapping("/add-friend")
	    public String addFriend(@ModelAttribute("addressBookDao") AddressBookDao addressBookDao) {
	        String response = addressBookService.addFriend(addressBookDao);
	        return "redirect:/friend/v1/get-all-friends";
	    }

	    @GetMapping("/display-add-friend-form")
	    public String displayAddFriendForm(Model model) {
	    	AddressBookDao addressBookDao = new AddressBookDao();
	        model.addAttribute("addressBookDao", addressBookDao);
	        return "add-friend-form";
	    }

	    @GetMapping("/get-all-friends")
	    public String getAllFriends(Model model) {
	        model.addAttribute("addressBookList", addressBookService.getAllFriends());
	        return "index";

	    }
	    
	    @GetMapping("/get-all-unique-friends")
	    public String getAllUniqueFriends(Model model) {
	        List<Friend> addressBookList = addressBookService.getAllUniqueFriends("AddressBook1", "AddressBook2");
	        model.addAttribute("addressBookList", addressBookList);
	        return "all-unique-friends";
	    }
	    
	    @GetMapping("/get-all-friends-by-addressbook-name")
	    public String getAllFriendsByAddressBookName(@ModelAttribute("addressBookName") String addressBookName, Model model) {
	        List<AddressBookDao> addressBookList = addressBookService.getAllFriendsByAddressBookName(addressBookName);
	        model.addAttribute("addressBookList", addressBookList);
	        return "all-friends-by-addressbook";
	    }

	    @GetMapping("/display-friend-form-by-addressbook")
	    public String displayFriendFormByAddressBook(Model model) {
	        // create model attribute to bind form data
	        AddressBookDao addressBookDao = new AddressBookDao();
	        model.addAttribute("addressBookDao", addressBookDao);
	        return "display-friend-form-by-addressbook";
	    }

	   
}
