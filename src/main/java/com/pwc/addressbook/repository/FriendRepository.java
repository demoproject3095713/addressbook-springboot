package com.pwc.addressbook.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pwc.addressbook.model.Friend;

@Repository
public interface FriendRepository extends JpaRepository<Friend, Integer> {
	
	    List<Friend> findByAddressBookName(String addressBookName);
	   
	    @Query(value = "SELECT * FROM FRIEND_TABLE ORDER BY name ASC", nativeQuery = true)
	    List<Friend> findAllFriends();

	    @Query(value = "SELECT * FROM FRIEND_TABLE WHERE ADDRESS_BOOK_NAME=?1 ORDER BY name ASC", nativeQuery = true)
	    List<Friend> findAllFriendsByAddressBookName(String addressBookName);

	    @Query(value = "SELECT * FROM FRIEND_TABLE  WHERE ADDRESS_BOOK_NAME = ?2 AND NAME = ?1", nativeQuery = true)
	    Optional<Friend> findByNameAndAddressBookName(String name, String addressBookName);
 

}
