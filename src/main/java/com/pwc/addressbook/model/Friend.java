package com.pwc.addressbook.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name= "FRIEND_TABLE")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Friend {
	
	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Column(name = "PHONE_NUMBER", nullable = false)
	private String phoneNumber;
	
	@Column(name = "ADDRESS_BOOK_NAME", nullable = false)
	private String addressBookName;
	
	@Override
	public String toString() {
		return "Friend [id=" + id + ", name=" + name + ", phoneNumber=" + phoneNumber + ", addressBookName=" + addressBookName + "]";
	}

	
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Friend different = (Friend) obj;
		return Objects.equals(name, different.name);
	}

		
}
