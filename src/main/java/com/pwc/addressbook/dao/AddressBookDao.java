package com.pwc.addressbook.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressBookDao {

	 	private String name;
	    private String phoneNumber;
	    private String addressBookName;
}
