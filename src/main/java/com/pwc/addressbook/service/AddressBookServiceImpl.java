package com.pwc.addressbook.service;


import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.addressbook.dao.AddressBookDao;
import com.pwc.addressbook.exception.ContactAlreadyExistsException;
import com.pwc.addressbook.exception.NoSuchAddressBookExistsException;
import com.pwc.addressbook.model.Friend;
import com.pwc.addressbook.repository.FriendRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class AddressBookServiceImpl implements AddressBookService {

    private static final Logger log = LoggerFactory.getLogger(AddressBookServiceImpl.class);
    @Autowired
    private FriendRepository friendRepository;
    @Autowired
    private ModelMapper modelMapper;

    public int getMaxId() {
        return friendRepository.findAll().size() + 1;
    }

    public String addFriend(AddressBookDao addressBookDao) {
        Optional<Friend> FriendExisting = friendRepository.findByNameAndAddressBookName(addressBookDao.getName(), addressBookDao.getAddressBookName());
        if (Optional.empty().equals(FriendExisting)) {
            Friend friend = new Friend();
            friend.setId(getMaxId());
            friend.setAddressBookName(addressBookDao.getAddressBookName());           
            friend.setName(addressBookDao.getName());
            friend.setPhoneNumber(addressBookDao.getPhoneNumber());
            friendRepository.save(friend);
            log.info("Contact Saved Successfully!");
            return "SUCCESS,Contact Saved";
        } else {
            throw new ContactAlreadyExistsException("Contact already present in Friend");
        }
    }

    public List<AddressBookDao> getAllFriends() {
        List<Friend> FriendList = friendRepository.findAll();
        if (!FriendList.isEmpty()) {
            List<AddressBookDao> addressBookDaoList = new ArrayList<>();

            for (Friend Friend : FriendList) {
            	addressBookDaoList.add(modelMapper.map(Friend, AddressBookDao.class));
            }
            log.info("fetching all contacts from DB!");
            return addressBookDaoList;
        } else {
            throw new NoSuchAddressBookExistsException("Address Book is Empty");
        }
    }


    public List<Friend> getAllUniqueFriends(String addressBookName1, String addressBookName2) {

        List<Friend> FriendList1 = friendRepository.findByAddressBookName(addressBookName1);
        List<Friend> FriendList2 = friendRepository.findByAddressBookName(addressBookName2);
        if (!FriendList2.isEmpty() && !FriendList1.isEmpty()) {
            List<Friend> findCommon = new ArrayList<>(FriendList1);
            findCommon.retainAll(FriendList2);
            FriendList1.removeAll(findCommon);
            FriendList2.removeAll(findCommon);
            FriendList1.addAll(FriendList2);
            List<Friend> uniqueList = new ArrayList<>(FriendList1);
            log.info("Fetching Unique contacts!");
            return uniqueList;
        } else {
            throw new NoSuchAddressBookExistsException("UniqueContacts cannot be found as either or one or both address books are empty!");
        }
    }

    

    public List<AddressBookDao> getAllFriendsByAddressBookName(String addressBookName) {
        if (addressBookName != null) {
            List<Friend> FriendList = friendRepository.findAllFriendsByAddressBookName(addressBookName);

            if (!FriendList.isEmpty()) {
                List<AddressBookDao> addressBookDaoList = new ArrayList<>();

                for (Friend Friend : FriendList) {
                	addressBookDaoList.add(modelMapper.map(Friend, AddressBookDao.class));
                }
                log.info("Fetching contacts from DB by Friend name!");
                return addressBookDaoList;
            } else {
                throw new NoSuchAddressBookExistsException("Not a valid Address Book Name:" + addressBookName);

            }
        } else {
            throw new NoSuchAddressBookExistsException("Address Book Name is null");
        }
    }
		
}



