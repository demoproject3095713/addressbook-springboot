package com.pwc.addressbook.service;

import java.util.List;

import com.pwc.addressbook.dao.AddressBookDao;
import com.pwc.addressbook.model.Friend;

public interface AddressBookService {
	
	 	public String addFriend(AddressBookDao addressBookDao);	   

	    public List<AddressBookDao> getAllFriends();	    

	    public List<Friend> getAllUniqueFriends(String addressBookName1, String addressBookName2);
	    
	    public List<AddressBookDao> getAllFriendsByAddressBookName(String addressBookName);

}
