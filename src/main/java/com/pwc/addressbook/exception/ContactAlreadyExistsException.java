package com.pwc.addressbook.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContactAlreadyExistsException extends RuntimeException {
    private String message;
}
