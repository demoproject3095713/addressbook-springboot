package com.pwc.addressbook.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NoSuchAddressBookExistsException extends RuntimeException {
    private String message;

}
